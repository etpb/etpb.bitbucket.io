.. _ImagingLinks:


Some useful links
=================

Local imaging resources
-----------------------
* `FMRIF <https://fmrif.nimh.nih.gov/>`_: scan/tech schedules,summer course videos
* `NMRF <https://intranet.nmrf.nih.gov/index.php>`_ local NIH only, has useful page of links

MRI
----
* `The Basics of MRI <http://www.cis.rit.edu/htbooks/mri/inside.htm>`_
* `Taxonomy of MRI sequences <http://pubs.rsna.org/doi/suppl/10.1148/rg.e24>`_

fMRI
-----
* `fMRI for newbies <http://www.fmri4newbies.com/>`_
* physical book:  `Huettel <https://www.amazon.com/Functional-Magnetic-Resonance-Imaging-Second/dp/0878932860>`_
* `AFNI <https://afni.nimh.nih.gov>`_ has lots of it's own documentation
* `psychopy <http://www.psychopy.org/>`_

Linux
-----
there are many
